#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable


#ifdef VERTEX_SHADER

// uniform bindings
layout(set = 0, binding = 0) uniform Camera {
    mat4 view_projection;
} camera;

layout(push_constant) uniform Constants {
	vec4 color;
} constants;

// input bindings
layout(location = 0) in vec3 ws_pos;

// output bindings
layout(location = 0) out vec4 frag_color;

void main()
{
	vec4 pos = camera.view_projection * vec4(ws_pos, 1.0);
	gl_Position = pos;
	frag_color = constants.color;
}

#endif // VERTEX_SHADER


#ifdef FRAGMENT_SHADER

// input bindings
layout(location = 0) in vec4 in_color;

// output bindings
layout(location = 0) out vec4 out_color;

void main()
{
    out_color = in_color;
}

#endif // FRAGMENT_SHADER
