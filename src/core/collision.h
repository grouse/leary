struct CollidableAABB {
    Vector3 position;
    Vector3 scale;
    bool colliding = false;
};

struct CollidableSphere {
    Vector3 position;
    f32 radius;
    f32 radius_sq;
    
    // TODO(jesper): these need to be nuked, they're only here to try things out
    bool colliding = false;
    bool selected = false;
};

struct Collision {
    DynamicArray<CollidableAABB> aabbs;
    DynamicArray<CollidableSphere> spheres;
};

struct DebugCollision {
    bool render_collidables = true;

    struct {
        VulkanBuffer vbo;
        i32 vertex_count = 0;
    } cube;

    struct {
        VulkanBuffer vbo;
        VulkanBuffer ibo;
        i32 vertex_count = 0;
    } sphere;
};
