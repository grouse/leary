/**
 * file:    gui.cpp
 * created: 2018-01-31
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2018 - all rights reserved
 */

static GuiRenderItem
gui_frame_render_item(Vector2 position, f32 width, f32 height, Vector4 color);

struct GuiWindowState {
    Vector2 pos;
    Vector2 size;
};
    
struct {
    GuiId hot = GUI_ID_INVALID;
    GuiId active = GUI_ID_INVALID;
    
    GuiId current_window = GUI_ID_INVALID;
    
    RHHashMap<GuiId, GuiWindowState> windows;
    
    f32 hot_z = -F32_MAX;
    
    VulkanBuffer vbo;
    usize vbo_offset = 0;
    void *vbo_map = nullptr;
    
    Vector2 mouse_delta = {};
    
    Vector2 left_mouse_down_pos = {};
    bool left_mouse_down = false;
    bool left_mouse_was_down = false;
    bool mouse_moved = false;
    
    DynamicArray<GuiRenderItem> render_queue_3d = {};
    DynamicArray<f32> render_queue_3d_z = {};
      
    DynamicArray<GuiRenderItem> render_queue = {};
    DynamicArray<GuiRenderItem> tooltip_render_queue = {};
    DynamicArray<InputEvent> input_queue = {};
} g_gui;

void gui_hot(GuiId id)
{
    g_gui.hot = id;
    g_gui.hot_z = -F32_MAX;
}

void gui_hot(GuiId id, f32 z)
{
    g_gui.hot = id;
    g_gui.hot_z = z;
}

void init_gui()
{
    init_map(&g_gui.windows, g_heap);
    
    g_gui.render_queue_3d.allocator = g_frame;
    g_gui.render_queue_3d_z.allocator = g_frame;
    g_gui.render_queue.allocator = g_frame;
    g_gui.tooltip_render_queue.allocator = g_frame;
    g_gui.input_queue.allocator = g_frame;
    
    // TODO(jesper): investigate whether we're better off with a device local buffer
    // plus staging transfer buffer for the GUI rendering
    g_gui.vbo = create_buffer(
        2*1024*1024, 
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    
    VkResult result = vkMapMemory(
        g_vulkan->handle,
        g_gui.vbo.memory,
        0, VK_WHOLE_SIZE, 0,
        &g_gui.vbo_map);

    ASSERT(result == VK_SUCCESS);
}

void destroy_gui()
{
    destroy_buffer(g_gui.vbo);
}

void gui_begin_frame()
{
    g_gui.mouse_delta = {};
    g_gui.left_mouse_was_down = g_gui.left_mouse_down;
    g_gui.mouse_moved = false;
}

void gui_input(InputEvent event)
{
    switch (event.type) {
    case InputType_mouse_press:
        g_gui.left_mouse_down = true;
        g_gui.left_mouse_down_pos = { event.mouse.x, event.mouse.y };
        break;
    case InputType_mouse_release:
        g_gui.left_mouse_down = false;
        break;
    case InputType_mouse_move:
        g_gui.mouse_delta += Vector2{ event.mouse.dx, event.mouse.dy };
        g_gui.mouse_moved = true;
        break;
    default: break;
    }
    array_add(&g_gui.input_queue, event);
}

GfxBuffer gui_push_vertices(void *data, i32 size)
{
    ASSERT(g_gui.vbo_offset + size < g_gui.vbo.size);    
    
    GfxBuffer buffer = {};
    buffer.size = size;
    buffer.offset = g_gui.vbo_offset;
    buffer.vk_buffer = g_gui.vbo.handle;

    memcpy((u8*)g_gui.vbo_map + g_gui.vbo_offset, data, size);
    g_gui.vbo_offset += size;
    
    return buffer;
}

void gui_draw_rect(Vector2 pos, Vector2 size, Vector4 color)
{
    struct Vertex {
        Vector2 pos;
        Vector4 color;
    };
    
    size.x = size.x / g_vulkan->resolution.x * 2.0f;
    size.y = size.y / g_vulkan->resolution.y * 2.0f;
    
    Vertex vertices[] = {
        { { 0.0f, 0.0f }, color }, { { 0.0f, size.y }, color }, { size,           color },
        { size,           color }, { { size.x, 0.0f }, color }, { { 0.0f, 0.0f }, color }
    };
  
    GuiRenderItem item = {};
    item.pipeline_id = Pipeline_gui_basic;
    item.vbo = gui_push_vertices(vertices, sizeof vertices);
    item.vertex_count = 6;

    Matrix4 t = translate(matrix4_identity(), camera_from_screen(pos));
    item.push_constants = gfx_push_constants(&t, sizeof t);

    array_add(&g_gui.render_queue, item);
}

void gui_draw_triangle(Vector2 p0, Vector2 p1, Vector2 p2, Vector4 color)
{
    struct Vertex {
        Vector2 pos;
        Vector4 color;
    };

    Vertex vertices[] = {
        { camera_from_screen(p0), color }, { camera_from_screen(p1), color }, { camera_from_screen(p2), color }
    };
    
    GuiRenderItem item = {};
    item.pipeline_id = Pipeline_gui_basic;
    item.vbo = gui_push_vertices(vertices, sizeof vertices);
    item.vertex_count = 3;
    
    Matrix4 identity = matrix4_identity();
    item.push_constants = gfx_push_constants(&identity, sizeof identity);
    array_add(&g_gui.render_queue, item);
}

void gui_draw_text(Vector2 pos, StringView text, Vector4 color)
{
    gui_textbox(text, color, &pos);
}

void gui_image(GuiFrame *frame, GfxTexture texture, Vector2 size, Vector2 *pos)
{
    GfxDescriptorSet set = gfx_push_descriptor(Pipeline_basic2d, 0);
    gfx_set_texture(Pipeline_basic2d, set, 0, texture);

    // TODO(jesper): figure out the name for this space and make it into a utility function
    Vector2 cs_size = size;
    cs_size.x = cs_size.x / g_settings.video.resolution.width;
    cs_size.y = cs_size.y / g_settings.video.resolution.height;

    f32 vertices[] = {
        0.0f,      0.0f,       0.0f, 0.0f,
        0.0f,      cs_size.y,  0.0f, 1.0f,
        cs_size.x, cs_size.y,  1.0f, 1.0f,

        cs_size.x, cs_size.y,  1.0f, 1.0f,
        cs_size.x, 0.0f,       1.0f, 0.0f,
        0.0f,      0.0f,       0.0f, 0.0f,
    };

    GuiRenderItem item = {};
    item.pipeline_id = Pipeline_basic2d;
    item.vbo = gui_push_vertices(vertices, sizeof vertices);
    item.vertex_count = 6;
    item.descriptors.allocator = g_frame;
    array_add(&item.descriptors, set);

    Matrix4 t = translate(matrix4_identity(), camera_from_screen(*pos));
    item.push_constants = gfx_push_constants(&t, sizeof t);

    array_add(&g_gui.render_queue, item);

    frame->width = max(frame->width, pos->x + size.x - frame->position.x);
    frame->height = max(frame->height, pos->y + size.y - frame->position.y);
}

void gui_image(GuiFrame *frame, GfxTexture texture, GfxBuffer vbo, Vector2 size, Vector2 *pos)
{
    GfxDescriptorSet set = gfx_push_descriptor(Pipeline_basic2d, 0);
    gfx_set_texture(Pipeline_basic2d, set, 0, texture);

    GuiRenderItem item = {};
    item.pipeline_id = Pipeline_basic2d;
    item.vbo = vbo;
    item.vertex_count = 6;
    item.descriptors.allocator = g_frame;
    array_add(&item.descriptors, set);

    Matrix4 t = translate(matrix4_identity(), camera_from_screen(*pos));
    item.push_constants = gfx_push_constants(&t, sizeof t);

    array_add(&g_gui.render_queue, item);

    frame->width = max(frame->width, pos->x + size.x - frame->position.x);
    frame->height = max(frame->height, pos->y + size.y - frame->position.y);
    pos->y += size.y;
}

void gui_image(GuiFrame *frame, GfxTexture texture, Vector2 *pos)
{
    gui_image(frame, texture, { (f32)texture.width, (f32)texture.height }, pos);
}

GuiTextbox gui_textbox_data(StringView text, Vector4 color, Vector2 *pos)
{
    PROFILE_FUNCTION();

    GuiTextbox tb = {};
    tb.position = { F32_MAX, F32_MAX };

    f32 bx = pos->x;
    
    pos->y += g_font.baseline;
    
    
    DynamicArray<GuiTextbox::Vertex> vertices = {};
    vertices.allocator = g_frame;
    array_reserve(&vertices, text.size * 6);

    for (i32 i = 0; i < text.size; i++) {
        char c = text[i];

        stbtt_aligned_quad q = {};

        {
            PROFILE_SCOPE(gui_textbox_GetBakedQuad);
            stbtt_GetBakedQuad(g_font.atlas, 1024, 1024, c, &pos->x, &pos->y, &q, 1);
        }

        {
            PROFILE_SCOPE(gui_textbox_widget_sizing);
            tb.position.x = min(tb.position.x, q.x0);
            tb.position.y = min(tb.position.y, q.y0);

            tb.size.x = max(tb.size.x, q.x1 - tb.position.x);
            tb.size.y = max(tb.size.y, q.y1 - tb.position.y);
        }

        GuiTextbox::Vertex v = {};
        v.position = { q.x0, q.y0 };
        v.uv       = { q.s0, q.t0 };
        v.color    = color;
        array_add(&vertices, v);

        v.position = { q.x0, q.y1 };
        v.uv       = { q.s0, q.t1 };
        v.color    = color;
        array_add(&vertices, v);
        
        v.position = { q.x1, q.y1 };
        v.uv       = { q.s1, q.t1 };
        v.color    = color;
        array_add(&vertices, v);
        
        v.position = { q.x1, q.y1 };
        v.uv       = { q.s1, q.t1 };
        v.color    = color;
        array_add(&vertices, v);

        v.position = { q.x1, q.y0 };
        v.uv       = { q.s1, q.t0 };
        v.color    = color;
        array_add(&vertices, v);

        v.position = { q.x0, q.y0 };
        v.uv       = { q.s0, q.t0 };
        v.color    = color;
        array_add(&vertices, v);
    }

    pos->x = bx;
    //pos->y += 20.0f;
    
    tb.vertices = vertices;
#if 0
    tb.vbo = gui_push_vertices(vertices.data, vertices.count * sizeof vertices[0]);
    tb.vertex_count = vertices.count;
#endif

    return tb;
}

GuiRenderItem gui_textbox_render_item(GuiTextbox tb)
{
    PROFILE_FUNCTION();

    GuiRenderItem item = {};
    item.pipeline_id = Pipeline_font;
    item.descriptors.allocator = g_frame;
    array_add(&item.descriptors, g_font.descriptor_set);

    item.vbo          = gui_push_vertices(tb.vertices.data, tb.vertices.count * sizeof tb.vertices[0]);
    item.vertex_count = tb.vertices.count;

    Matrix4 t = matrix4_identity();

    t[0][0] = 2.0f / g_vulkan->resolution.x;
    t[1][1] = 2.0f / g_vulkan->resolution.y;

    t[3][0] = -1.0f;
    t[3][1] = -1.0f;

    item.push_constants = gfx_push_constants(&t, sizeof t);
    return item;
}


void gui_render_items(VkCommandBuffer command, Array<GuiRenderItem> items)
{
    PROFILE_FUNCTION();

    for (i32 i = 0; i < items.count; i++) {
        auto &item = items[i];

        ASSERT(item.pipeline_id < Pipeline_count);
        VulkanPipeline &pipeline = g_vulkan->pipelines[item.pipeline_id];

        vkCmdBindPipeline(
            command,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline.handle);

        if (item.descriptors.count > 0) {
            gfx_bind_descriptors(
                command,
                pipeline.layout,
                VK_PIPELINE_BIND_POINT_GRAPHICS,
                item.descriptors);
        }

        if (item.push_constants.size > 0) {
            vkCmdPushConstants(
                command,
                pipeline.layout,
                item.push_constants.stages,
                item.push_constants.offset,
                item.push_constants.size,
                item.push_constants.data);
        }

        vkCmdBindVertexBuffers(command, 0, 1, &item.vbo.vk_buffer, (VkDeviceSize*)&item.vbo.offset);
        vkCmdDraw(command, item.vertex_count, 1, 0, 0);
    }
}

void gui_render(VkCommandBuffer command)
{
    PROFILE_FUNCTION();
    
    for (i32 i = 0; i < g_gui.render_queue_3d.count; i++) {
        for (i32 j = i+1; j < g_gui.render_queue_3d.count; j++) {
            if (g_gui.render_queue_3d_z[i] > g_gui.render_queue_3d_z[j]) {
                std::swap(g_gui.render_queue_3d_z[i], g_gui.render_queue_3d_z[j]);
                std::swap(g_gui.render_queue_3d[i], g_gui.render_queue_3d[j]);
            }
        }
    }

    gui_render_items(command, g_gui.render_queue_3d);
    gui_render_items(command, g_gui.render_queue);
    gui_render_items(command, g_gui.tooltip_render_queue);
    
    reset_array(&g_gui.render_queue_3d_z);
    reset_array(&g_gui.render_queue_3d);
    reset_array(&g_gui.render_queue);
    reset_array(&g_gui.tooltip_render_queue);
    reset_array(&g_gui.input_queue);
    g_gui.vbo_offset = 0;
}

GuiTextbox gui_textbox(StringView text, Vector4 color, Vector2 *pos)
{
    PROFILE_FUNCTION();

    GuiTextbox tb = gui_textbox_data(text, color, pos);
#if 1
    GuiRenderItem item = gui_textbox_render_item(tb);

    array_add(&g_gui.render_queue, item);
#endif
    return tb;
}

GuiTextbox gui_textbox(
    GuiFrame *frame,
    StringView text,
    Vector4 color,
    Vector2 *pos)
{
    PROFILE_FUNCTION();

    GuiTextbox tb = gui_textbox(text, color, pos);

    frame->position.x = min(frame->position.x, tb.position.x);
    frame->position.y = min(frame->position.y, tb.position.y);

    frame->width = max(
        frame->width,
        tb.position.x + tb.size.x - frame->position.x);

    frame->height = max(
        frame->height,
        tb.position.y + tb.size.y - frame->position.y);

    return tb;
}

GuiTextbox gui_textbox(
    GuiFrame *frame,
    StringView text,
    Vector4 color,
    Vector4 highlight,
    Vector2 *pos)
{
        (void)highlight;
    PROFILE_FUNCTION();

    GuiTextbox tb = gui_textbox(frame, text, color, pos);
#if 0
        // TODO(jesper): figure out highlight
    if (is_mouse_over(tb)) {
        for (i32 i = 0; i < tb.vertices_count; i++) {
            tb.vertices[i].color = highlight;
        }
    }
#endif
    return tb;
}

bool is_pressed(GuiWidget widget)
{
    PROFILE_FUNCTION();

    Vector2 tl = widget.position;
    Vector2 br = widget.position + widget.size;

    for (auto event : g_gui.input_queue) {
        if (event.type == InputType_mouse_press) {
            if (event.mouse.x >= tl.x && event.mouse.x <= br.x &&
                event.mouse.y >= tl.y && event.mouse.y <= br.y)
            {
                return true;
            }
        }
    }

    return false;
}

bool is_pressed(GuiTextbox textbox)
{
    PROFILE_FUNCTION();

    Vector2 tl = textbox.position;
    Vector2 br = textbox.position + textbox.size;

    for (auto event : g_gui.input_queue) {
        if (event.type == InputType_mouse_press) {
            if (event.mouse.x >= tl.x && event.mouse.x <= br.x &&
                event.mouse.y >= tl.y && event.mouse.y <= br.y)
            {
                return true;
            }
        }
    }

    return false;
}

bool is_mouse_over(GuiTextbox textbox)
{
    PROFILE_FUNCTION();

    Vector2 tl = textbox.position;
    Vector2 br = textbox.position + textbox.size;

    Vector2 mouse = get_mouse_position();

    if (mouse.x >= tl.x && mouse.x <= br.x &&
        mouse.y >= tl.y && mouse.y <= br.y)
    {
        return true;
    }

    return false;
}

GuiFrame gui_frame_begin(Vector4 color)
{
    GuiFrame frame = {};
    frame.color        = color;
    frame.render_index = array_add(&g_gui.render_queue, {});
    return frame;
}

GuiFrame gui_frame_begin(Vector2 position, Vector4 color)
{
    GuiFrame frame = {};
    frame.position     = position;
    frame.color        = color;
    frame.render_index = array_add(&g_gui.render_queue, {});
    return frame;
}

void gui_frame_end(GuiFrame frame)
{
    auto item = gui_frame_render_item(
        frame.position,
        frame.width, frame.height,
        frame.color);
    g_gui.render_queue[frame.render_index] = item;
}

void gui_frame(Vector2 position, f32 width, f32 height, Vector4 color)
{
    auto item = gui_frame_render_item(position, width, height, color);
    array_add(&g_gui.render_queue, item);
}

GuiRenderItem
gui_frame_render_item(Vector2 position, f32 width, f32 height, Vector4 color)
{
    PROFILE_FUNCTION();

    struct Vertex {
        Vector2 position;
        Vector4 color;
    };

    GuiRenderItem item = {};
    item.pipeline_id = Pipeline_gui_basic;

    width  = width / g_vulkan->resolution.x * 2.0f;
    height = height / g_vulkan->resolution.y * 2.0f;

    Vertex tl, tr, br, bl;
    tl.position = Vector2{ 0.0f,  0.0f };
    tr.position = Vector2{ width, 0.0f };
    br.position = Vector2{ width, height };
    bl.position = Vector2{ 0.0f,  height };

    tl.color = tr.color = br.color = bl.color = color;

    DynamicArray<Vertex> vertices = {};
    vertices.allocator = g_frame;
    
    array_add(&vertices, tl);
    array_add(&vertices, bl);
    array_add(&vertices, br);

    array_add(&vertices, br);
    array_add(&vertices, tr);
    array_add(&vertices, tl);

    item.vbo = gui_push_vertices(vertices.data, vertices.count * sizeof vertices[0]);
    item.vertex_count = vertices.count;

    Matrix4 t = translate(matrix4_identity(), camera_from_screen(position));
    item.push_constants = gfx_push_constants(&t, sizeof t);

    return item;
}

void gui_tooltip(StringView text, Vector4 fg, Vector4 bg)
{
    PROFILE_FUNCTION();

    Vector2 mouse_pos = get_mouse_position();

    GuiFrame frame = {};
    frame.position = mouse_pos;
    frame.color = bg;

    GuiTextbox tb = gui_textbox_data(text, fg, &mouse_pos);
    GuiRenderItem text_item = gui_textbox_render_item(tb);

    frame.position.x = min(frame.position.x, tb.position.x);
    frame.position.y = min(frame.position.y, tb.position.y);

    frame.width = max(
        frame.width,
        tb.position.x + tb.size.x - frame.position.x);

    frame.height = max(
        frame.height,
        tb.position.y + tb.size.y - frame.position.y);

    GuiRenderItem frame_item = gui_frame_render_item(
        frame.position,
        frame.width, frame.height,
        frame.color);

    array_add(&g_gui.tooltip_render_queue, frame_item);
    array_add(&g_gui.tooltip_render_queue, text_item);
}

void gui_mover_gizmo_axis(
    GuiId id,
    Vector3 *pos, 
    Vector4 color, 
    Vector2 size, 
    Vector2 tip_size,
    Vector3 plane, 
    Vector3 axis,
    Vector3 n)
{
    Camera *camera = &g_game->cameras[g_game->active_camera];
    Vector3 ws_camera_p = camera->position;
    Vector3 cs_camera_p = -ws_camera_p;

    Matrix4 cs_from_ws = matrix4(camera->rotation) * translate(matrix4_identity(), cs_camera_p);
    Matrix4 ws_from_cs = transpose(cs_from_ws);

    Vector3 br = *pos + 0.5f*size.x*plane + axis*size.y;
    Vector3 tr = *pos - 0.5f*size.x*plane + axis*size.y;
    Vector3 tl = *pos - 0.5f*size.x*plane;
    Vector3 bl = *pos + 0.5f*size.x*plane;
    
    Vector3 q4 = tr + 0.5f*(br-tr);
    Vector3 top = tr + tip_size.x*(tr-br);
    Vector3 bot = br + tip_size.x*(br-tr);
    Vector3 tip = q4 + axis*tip_size.y;

    Vector3 vertices[] = {
        // arrow strip
        br, tr, tl,
        tl, bl, br,
        
        // arrow tip
        tip, top, q4,
        q4, bot, tip
    };
    
    if (g_gui.active == id && !g_gui.left_mouse_down) {
        g_gui.active = GUI_ID_INVALID;
    }
    
    if (g_gui.mouse_moved) {
        Vector3 ws_direction = ws_ray_dir_from_ss(get_mouse_position(), ws_from_cs);

        f32 t = ray_vs_rect(ws_camera_p, ws_direction, br, tr, tl, bl);
        t = t < F32_MAX ? t : ray_vs_triangle(ws_camera_p, ws_direction, tip, top, bot);

        Vector4 ws_p = vector4(ws_camera_p + t*ws_direction, 1.0f);
        f32 z = (cs_from_ws * ws_p).z;

        if (g_gui.hot == id) {
            if (t < F32_MAX || g_gui.active == id) {
                gui_hot(id, z);
            } else {
                gui_hot(GUI_ID_INVALID);
            }
        } else if (g_gui.hot == GUI_ID_INVALID && t < F32_MAX && z > g_gui.hot_z) {
            gui_hot(id, z);
        }
    }
    
    if (g_gui.hot == id && 
        g_gui.active != id && 
        g_gui.left_mouse_down && 
        !g_gui.left_mouse_was_down)
    {
        Vector3 ws_direction = ws_ray_dir_from_ss(get_mouse_position(), ws_from_cs);

        if (ray_vs_rect(ws_camera_p, ws_direction, br, tr, tl, bl) < F32_MAX ||
            ray_vs_triangle(ws_camera_p, ws_direction, tip, top, bot) < F32_MAX)
        {
            g_gui.active = id;
        }
    }
    
    if (g_gui.mouse_moved && g_gui.active == id) {
        Vector2 mouse_pos = get_mouse_position();
        Vector2 prev_mouse_pos = mouse_pos - g_gui.mouse_delta;
        
        Vector3 ws_dir = ws_ray_dir_from_ss(mouse_pos, ws_from_cs);
        Vector3 ws_prev_dir = ws_ray_dir_from_ss(prev_mouse_pos, ws_from_cs);
        
        f32 t = ray_vs_plane(ws_camera_p, ws_dir, n, dot(n, *pos));
        f32 prev_t = ray_vs_plane(ws_camera_p, ws_prev_dir, n, dot(n, *pos));
        
        if (t < F32_MAX && prev_t < F32_MAX) {
            ASSERT(t < F32_MAX);
            ASSERT(prev_t < F32_MAX);

            Vector3 ws_p = t * ws_dir;
            Vector3 ws_prev_p = prev_t * ws_prev_dir;

            Vector3 diff = ws_p - ws_prev_p;
            *pos += axis*dot(diff, axis);
        }
    }
    
    if (g_gui.hot == id) {
        color = { 1.0f, 0.0f, 1.0f, 1.0f };
    }
    
    if (g_gui.active == id) {
        color = { 1.0f, 1.0f, 1.0f, 1.0f };
    }
    
    {
        GuiRenderItem item = {};
        item.pipeline_id = Pipeline_gizmo;
        item.vbo = gui_push_vertices(vertices, sizeof vertices);
        item.vertex_count = ARRAY_SIZE(vertices);
        item.push_constants = gfx_push_constants(&color, sizeof color);
        item.descriptors.allocator = g_frame;
        
        array_add(&item.descriptors, g_vulkan->camera_set);
        
        f32 z_value = (cs_from_ws * vector4(tr, 1.0f)).z;

        array_add(&g_gui.render_queue_3d, item);
        array_add(&g_gui.render_queue_3d_z, z_value);
    }
}

void gui_mover_gizmo_plane(
    GuiId id,
    Vector3 *pos, 
    Vector4 color,
    f32 size,
    f32 offset,
    Vector3 axis0,
    Vector3 axis1,
    Vector3 n)
{
    Camera *camera = &g_game->cameras[g_game->active_camera];
    Vector3 ws_camera_p = camera->position;
    Vector3 cs_camera_p = -ws_camera_p;

    Matrix4 cs_from_ws = matrix4(camera->rotation) * translate(matrix4_identity(), cs_camera_p);
    Matrix4 ws_from_cs = transpose(cs_from_ws);
    
    Vector3 bl = *pos + offset*axis0 + offset*axis1;
    Vector3 br = bl + size*axis0;
    Vector3 tr = bl + size*axis0 + size*axis1;
    Vector3 tl = bl + size*axis1;

    Vector3 vertices[] = {
        br, tr, tl, 
        tl, bl, br
    };

    if (g_gui.active == id && !g_gui.left_mouse_down) {
        g_gui.active = GUI_ID_INVALID;
    }

    if (g_gui.mouse_moved) {
        Vector3 ws_direction = ws_ray_dir_from_ss(get_mouse_position(), ws_from_cs);

        f32 t = ray_vs_rect(ws_camera_p, ws_direction, br, tr, tl, bl);
        t = t < F32_MAX ? t : ray_vs_rect(ws_camera_p, ws_direction, bl, tl, tr, br);
        
        Vector4 ws_p = vector4(ws_camera_p + t*ws_direction, 1.0f);
        f32 z = (cs_from_ws * ws_p).z;

        if (g_gui.hot == id) {
            if (t < F32_MAX || g_gui.active == id ) {
                gui_hot(id, z);
            } else {
                gui_hot(GUI_ID_INVALID);
            }
        } else if (g_gui.hot == GUI_ID_INVALID && t < F32_MAX && z > g_gui.hot_z) {
            gui_hot(id, z);
        }
    }

    if (g_gui.hot == id && 
        g_gui.active != id && 
        g_gui.left_mouse_down && 
        !g_gui.left_mouse_was_down)
    {
        Vector3 ws_direction = ws_ray_dir_from_ss(get_mouse_position(), ws_from_cs);

        if (ray_vs_rect(ws_camera_p, ws_direction, br, tr, tl, bl) < F32_MAX ||
            ray_vs_rect(ws_camera_p, ws_direction, bl, tl, tr, br))
        {
            g_gui.active = id;
        }
    }

    if (g_gui.mouse_moved && g_gui.active == id) {
        Vector2 mouse_pos = get_mouse_position();
        Vector2 prev_mouse_pos = mouse_pos - g_gui.mouse_delta;

        Vector3 ws_dir = ws_ray_dir_from_ss(mouse_pos, ws_from_cs);
        Vector3 ws_prev_dir = ws_ray_dir_from_ss(prev_mouse_pos, ws_from_cs);

        f32 t = ray_vs_plane(ws_camera_p, ws_dir, n, dot(n, *pos));
        t = t < F32_MAX ? t : ray_vs_plane(ws_camera_p, ws_dir, -n, -dot(n, *pos));
        
        f32 prev_t = ray_vs_plane(ws_camera_p, ws_prev_dir, n, dot(n, *pos));
        prev_t = prev_t < F32_MAX ? prev_t : ray_vs_plane(ws_camera_p, ws_prev_dir, -n, -dot(n, *pos));
        
        if (t < F32_MAX && prev_t < F32_MAX) {
            Vector3 ws_p = t * ws_dir;
            Vector3 ws_prev_p = prev_t * ws_prev_dir;

            Vector3 diff = ws_p - ws_prev_p;
            *pos += comp_wise_mul(diff, axis0 + axis1);
        }
    }
    
    if (g_gui.hot == id) {
        color = { 1.0f, 0.0f, 1.0f, 1.0f };
    }

    if (g_gui.active == id) {
        color = { 1.0f, 1.0f, 1.0f, 1.0f };
    }

    {
        GuiRenderItem item = {};
        item.pipeline_id = Pipeline_gizmo;
        item.vbo = gui_push_vertices(vertices, sizeof vertices);
        item.vertex_count = ARRAY_SIZE(vertices);
        item.push_constants = gfx_push_constants(&color, sizeof color);
        item.descriptors.allocator = g_frame;
        
        array_add(&item.descriptors, g_vulkan->camera_set);
        
        f32 z_value = (cs_from_ws * vector4(tr, 1.0f)).z;
        array_add(&g_gui.render_queue_3d, item);
        array_add(&g_gui.render_queue_3d_z, z_value);
    }
}

void gui_mover_gizmo(Vector3 *pos, Camera *camera)
{
    Vector3 camera_p = camera->position;
    
#if 0
    if (g_game->active_camera == CAMERA_PLAYER) {
        EntityID player_id = find_entity_id("player.ent");
        Entity &player = g_entities[player_id];
        camera_p += player.position;
    }
#endif
    
    Vector4 x_color = { 1.0f, 0.0f, 0.0f, 1.0f };
    Vector4 y_color = { 0.0f, 1.0f, 0.0f, 1.0f };
    Vector4 z_color = { 0.0f, 0.0f, 1.0f, 1.0f };
    
    Vector2 size = { 0.05f, 1.0f };
    Vector2 tip_size = { 0.5f, 0.1f };
    
    Vector3 c = camera_p - *pos;
    
    Vector3 yz = normalise(Vector3{ 0.0f, -c.z, c.y });
    Vector3 zx = normalise(Vector3{ c.z, 0.0f, -c.x });
    Vector3 xy = normalise(Vector3{ -c.y, c.x, 0.0f });
    
    GuiId id = { 1, -1 };
    Vector3 x_norm = normalise(Vector3{ 0.0f, c.y, c.z });
    Vector3 y_norm = normalise(Vector3{ c.x, 0.0f, c.z });
    Vector3 z_norm = normalise(Vector3{ c.x, c.y, 0.0f });
    
    Vector3 x_axis = { 1.0f, 0.0f, 0.0f };
    Vector3 y_axis = { 0.0f, 1.0f, 0.0f };
    Vector3 z_axis = { 0.0f, 0.0f, 1.0f };
    
    Vector4 rect_color = { 0.4f, 0.4f, 0.4f, 0.4f };
    f32 rect_offset = size.x + 0.1f;
    f32 rect_size = 0.25f * size.y;
    
    gui_mover_gizmo_axis(GUI_ID_INTERNAL(id, 0), pos, x_color, size, tip_size, yz, x_axis, x_norm);
    gui_mover_gizmo_axis(GUI_ID_INTERNAL(id, 1), pos, y_color, size, tip_size, zx, y_axis, y_norm);
    gui_mover_gizmo_axis(GUI_ID_INTERNAL(id, 2), pos, z_color, size, tip_size, xy, z_axis, z_norm); 

    gui_mover_gizmo_plane(
        GUI_ID_INTERNAL(id, 3), 
        pos, 
        rect_color, rect_size, rect_offset, 
        x_axis, y_axis, { 0.0f, 0.0f, 1.0f });
    
    gui_mover_gizmo_plane(
        GUI_ID_INTERNAL(id, 4), 
        pos, 
        rect_color, rect_size, rect_offset, 
        z_axis, x_axis, { 0.0f, 1.0f, 0.0f });
    
    gui_mover_gizmo_plane(
        GUI_ID_INTERNAL(id, 5), 
        pos, 
        rect_color, rect_size, rect_offset, 
        y_axis, z_axis, { 1.0f, 0.0f, 0.0f });
}

void gui_begin_window(GuiId id, StringView title, Vector2 initial_pos, Vector2 initial_size)
{
    ASSERT(g_gui.current_window == GUI_ID_INVALID);
    ASSERT(g_gui.current_window != id);
    g_gui.current_window = id;
    
    GuiWindowState *state = map_find(&g_gui.windows, id);
    Vector2 pos = state ? state->pos : initial_pos;
    Vector2 size = state ? state->size : initial_size;
    
    Vector4 window_bg = { 0.0f, 0.0f, 0.0f, 1.0f };
    Vector2 window_margin = { 1.0f, 1.0f };
    
    Vector4 resize_bg = { 0.5f, 0.5f, 0.5f, 1.0f };
    Vector4 resize_hot_bg { 1.0f, 1.0f, 1.0f, 1.0f };

    Vector2 title_size { size.x - window_margin.x*2.0f, 25.0f };
    Vector4 title_bg = { 1.0f, 0.0f, 0.0f, 1.0f };
    Vector4 title_hot_bg = { 1.0f, 1.0f, 0.0f, 1.0f };
    Vector4 title_fg = { 1.0f, 1.0f, 1.0f, 1.0f };
    Vector2 title_margin = { 5.0f, 2.0f };
    
    if (!state) {
        map_add(&g_gui.windows, id, { initial_pos, initial_size });
    }
    
    GuiId title_id = GUI_ID_INTERNAL(id, 0);
    GuiId resize_id = GUI_ID_INTERNAL(id, 1);
    
    Vector2 resize_br = pos + size - window_margin;
    Vector2 resize_tr = pos + size - window_margin - Vector2{ 0.0f, 10.0f };
    Vector2 resize_bl = pos + size - window_margin - Vector2{ 10.0f, 0.0f };

    Vector2 mouse = get_mouse_position();
    Vector2 rel = mouse - pos - window_margin;
    if (g_gui.active == title_id || 
        (rel.x >= 0 && rel.x <= title_size.x && 
         rel.y >= 0 && rel.y <= title_size.y))
    {
        gui_hot(title_id);
    } else if (g_gui.hot == title_id && g_gui.active != title_id) {
        gui_hot(GUI_ID_INVALID);
    } else {
        if (g_gui.active == resize_id || 
            point_in_triangle(mouse, resize_br, resize_tr, resize_bl)) 
        {
            gui_hot(resize_id);
        } else if (g_gui.hot == resize_id && g_gui.active != resize_id) {
            gui_hot(GUI_ID_INVALID);
        }
    }

    if (g_gui.hot == title_id) {
        if (g_gui.active != title_id && g_gui.left_mouse_down && !g_gui.left_mouse_was_down) {
            g_gui.active = title_id;
        } else if (g_gui.active == title_id && !g_gui.left_mouse_down) {
            g_gui.active = GUI_ID_INVALID;
        } else if (g_gui.active == title_id && g_gui.mouse_moved) {
            pos += g_gui.mouse_delta;
        }
    } else if (g_gui.hot == resize_id) {
        if (g_gui.active != resize_id && g_gui.left_mouse_down && !g_gui.left_mouse_was_down) {
            g_gui.active = resize_id;
        } else if (g_gui.active == resize_id && !g_gui.left_mouse_down) {
            g_gui.active = GUI_ID_INVALID;
        } else if (g_gui.active == resize_id && g_gui.mouse_moved) {
            size += g_gui.mouse_delta;
            resize_br += g_gui.mouse_delta;
            resize_tr += g_gui.mouse_delta;
            resize_bl += g_gui.mouse_delta;
            title_size.x += g_gui.mouse_delta.x;
        }
    }
    
    gui_draw_rect(pos, size, window_bg);
    gui_draw_rect(pos + window_margin, title_size, g_gui.hot == title_id ? title_hot_bg : title_bg);
    gui_draw_text(pos + window_margin + title_margin, title, title_fg);
    gui_draw_triangle(resize_br, resize_tr, resize_bl, g_gui.hot == resize_id ? resize_hot_bg : resize_bg);
                  
    state = map_find(&g_gui.windows, id);
    ASSERT(state != nullptr);
    state->pos = pos;
    state->size = size;
}

void gui_end_window(GuiId id)
{
    ASSERT(g_gui.current_window == id);
    g_gui.current_window = GUI_ID_INVALID;
}