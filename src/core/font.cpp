/**
 * file:    font.cpp
 * created: 2017-11-11
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2017 - all rights reserved
 */

FontData g_font = {};
extern VulkanDevice *g_vulkan;

void init_fonts()
{
    usize font_size;
    FilePath font_path = resolve_file_path(GamePath_data, "fonts/Roboto-Regular.ttf", g_stack);
    if (font_path.absolute.size == 0) {
        return;
    }
    
    u8 *font_data = (u8*)read_file(font_path, &font_size, g_frame);

    stbtt_fontinfo font;
    stbtt_InitFont(&font, font_data, 0);

    u8 *bitmap = alloc_array(g_frame, u8, 1024*1024);
    stbtt_BakeFontBitmap(font_data, 0, 20.0f, bitmap, 1024, 1024, 0, 256, g_font.atlas);
    g_font.scale = stbtt_ScaleForPixelHeight(&font, 20);
    stbtt_GetFontVMetrics(&font, &g_font.ascent, 0, 0);
    g_font.baseline = (i32)(g_font.ascent * g_font.scale);

    VkComponentMapping components = {};
    components.a = VK_COMPONENT_SWIZZLE_R;
    
    TextureAsset *texture = add_texture("font-regular", 1024, 1024, VK_FORMAT_R8_UNORM, bitmap, components);
    
    g_font.descriptor_set = gfx_create_descriptor(Pipeline_font, 0);
    ASSERT(g_font.descriptor_set.vk_set != VK_NULL_HANDLE);
    
    gfx_set_texture(Pipeline_font, g_font.descriptor_set, 0, texture->gfx_texture);
}

void destroy_fonts()
{
}

