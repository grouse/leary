/**
 * file:    gui.h
 * created: 2018-02-03
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2018 - all rights reserved
 */

struct GuiId {
    i32 owner = -1;
    i32 index = -1;
    
    // NOTE(jesper): this is an internal value for composed GUI widgets
    i32 internal = -1; 
    
    bool operator==(const GuiId rhs)
    {
        return owner == rhs.owner && index == rhs.index && internal == rhs.internal;
    }
    
    bool operator!=(const GuiId rhs) { return !(*this == rhs); }
};

#define GUI_ID_INVALID GuiId{ -1, -1 }
#define GUI_ID_INTERNAL(id, val) GuiId{ id.owner, id.index, val }

struct GuiRenderItem
{
    Vector2                position;
    GfxBuffer              vbo;
    i32                    vertex_count;

    PipelineID              pipeline_id;
    DynamicArray<GfxDescriptorSet> descriptors;
    GfxPushConstants push_constants;
};

struct GuiFrame
{
    i32 render_index;
    Vector2 position;
    f32 width;
    f32 height;
    Vector4 color;
};

struct GuiWidget
{
    Vector2 size     = { 0.0f, 0.0f };
    Vector2 position = { 0.0f, 0.0f };
};

struct GuiTextbox
{
    struct Vertex {
        Vector2 position;
        Vector2 uv;
        Vector4 color;
    };

    Vector2 size     = { 0.0f, 0.0f };
    Vector2 position = { 0.0f, 0.0f };
    
    Array<Vertex> vertices;
};

bool is_mouse_over(GuiTextbox textbox);

void gui_image(GuiFrame *frame, GfxTexture texture, Vector2 *pos);
void gui_image(GuiFrame *frame, GfxTexture texture, Vector2 size, Vector2 *pos);
void gui_image(GuiFrame *frame, GfxTexture texture, GfxBuffer vbo, Vector2 size, Vector2 *pos);

GuiTextbox gui_textbox(StringView text, Vector4 color, Vector2 *pos);
GuiTextbox gui_textbox(GuiFrame *frame, StringView text, Vector4 color, Vector2 *pos);
