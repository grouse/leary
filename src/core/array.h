/**
 * file:    array.h
 * created: 2018-01-02
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2018 - all rights reserved
 */

// TODO(jesper): this is such a common pattern, that I should investigate
// wrangling C++ into doing this for me. maybe with an initializer list thing?
//DynamicArray<Vertex> vertices = {};
//vertices.allocator = g_frame;

//array_add(&vertices, tl);
//array_add(&vertices, bl);
//array_add(&vertices, br);

//array_add(&vertices, br);
//array_add(&vertices, tr);
//array_add(&vertices, tl);

template<typename T>
struct Array {
    T* data      = nullptr;
    i32 count    = 0;

    T& operator[] (i32 i)
    {
        ASSERT(i < count);
        ASSERT(i >= 0);
        return data[i];
    }

    // NOTE(jesper): ranged for compatibility
    T* begin() { return &data[0]; }
    T* end() { return &data[count]; }
};

template<typename T>
struct DynamicArray : Array<T> {
    i32 capacity = 0;
    Allocator *allocator = nullptr;
};

template<typename T>
i32 array_add(DynamicArray<T> *a, T e);

template<typename T>
Array<T> create_array(Allocator *allocator);
