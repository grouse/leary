/**
 * file:    array.cpp
 * created: 2017-03-10
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2017-2018 - all rights reserved
 */

template<typename T>
void array_reserve(DynamicArray<T> *arr, i32 capacity)
{
    if (arr->capacity < capacity) {
        // TODO(jesper): default allocator
        if (arr->allocator == nullptr) arr->allocator = g_heap;
        arr->data = realloc_array(arr->allocator, arr->data, capacity);
        arr->capacity = capacity;
    }
}

template<typename T>
void array_resize(DynamicArray<T> *arr, i32 capacity)
{
    if (arr->capacity != capacity) {
        // TODO(jesper): default allocator
        if (arr->allocator == nullptr) arr->allocator = g_heap;
        arr->data = realloc_array(arr->allocator, arr->data, capacity);
        arr->capacity = capacity;
    }
    
    arr->count = capacity;
}

template<typename T>
void destroy_array(DynamicArray<T> *a)
{
    if (a->capacity > 0) {
        ASSERT(a->allocator);
        
        dealloc(a->allocator, a->data);
        a->data     = nullptr;
        a->capacity = 0;
        a->count    = 0;
    }
}

template<typename T>
void reset_array(DynamicArray<T> *a)
{
    dealloc(a->allocator, a->data);
    a->count = a->capacity = 0;
    a->data = nullptr;
}

template<typename T>
i32 array_add(DynamicArray<T> *a, T e)
{
    // TODO(jesper): default allocator
    if (a->allocator == nullptr) a->allocator = g_heap;
    
    if (a->count == a->capacity) {
        i32 capacity = a->capacity == 0 ? 1 : a->capacity * 2;
        a->data      = realloc_array(a->allocator, a->data, capacity);
        a->capacity  = capacity;
    }

    a->data[a->count] = e;
    return a->count++;
}

// NOTE(jesper): I don't really like this name, but it's to avoid ambiguiety with
// struct literals in array_add, because C++ is garbage
template<typename T>
i32 array_merge(DynamicArray<T> *a, Array<T> a2)
{
    if (a->allocator == nullptr) a->allocator = g_heap;

    if (a->count + a2.count >= a->capacity) {
        i32 capacity = a->capacity == 0 ? 1 : a->capacity * 2;
        capacity = MAX(capacity, a2.count);
        a->data      = realloc_array(a->allocator, a->data, capacity);
        a->capacity  = capacity;
    }
    
    memcpy(&a->data[a->count], a2.data, a2.count);
    a->count += a2.count;
    return a->count;
}

template<typename T>
i32 array_remove(DynamicArray<T> *a, i32 i)
{
    if ((a->count - 1) == i) {
        return --a->count;
    }

    a->data[i] = a->data[--a->count];
    return a->count;
}

template<typename T>
i32 array_remove_ordered(DynamicArray<T> *a, i32 i)
{
    if ((a->count - 1) == i) {
        return --a->count;
    }

    std::memmove(&a->data[i], &a->data[i+1], (a->count-i-1) * sizeof(T));
    return --a->count;
}

template<typename T, typename F>
void array_insertion_sort(Array<T> *a, F cmp)
{
    for (i32 i = 0; i < a->count; i++) {
        for (i32 j = i;
             j > 0 && cmp(&a->data[j-1], &a->data[j]);
             j--)
        {
            std::swap(a->data[j], a->data[j-1]);
        }
    }
}
