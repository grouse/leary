/**
 * file:    font.h
 * created: 2018-01-31
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2018 - all rights reserved
 */

struct FontData
{
    stbtt_bakedchar atlas[256];
    i32 ascent;
    i32 baseline;
    f32 scale;
    GfxDescriptorSet descriptor_set;
};
