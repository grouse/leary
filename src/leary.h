/**
 * file:    leary.h
 * created: 2017-03-18
 * authors: Jesper Stefansson (jesper.stefansson@gmail.com)
 *
 * Copyright (c) 2017-2018 - all rights reserved
 */

#ifndef INTROSPECT
#define INTROSPECT
#endif

struct DebugOverlay {
    bool show_allocators = false;
    bool show_profiler   = false;
};

struct Entity {
    EntityID   id;
    Vector3    position;
    Vector3    scale;
    Quaternion rotation = Quaternion::make( Vector3{ 0.0f, 1.0f, 0.0f });
    MeshID mesh_id = ASSET_INVALID_ID;
    GfxDescriptorSet descriptor_set;
};

struct IndexRenderObject {
    i32            entity_id = -1;
    PipelineID     pipeline;
    struct {
        VulkanBuffer points;
        VulkanBuffer normals;
        VulkanBuffer tangents;
        VulkanBuffer bitangents;
        VulkanBuffer uvs;
    } vbo;
    VulkanBuffer   ibo;
    i32            index_count;
};

// TODO(jesper): stick these in the pipeline so that we reduce the number of
// pipeline binds
struct RenderObject {
    i32            entity_id = -1;
    PipelineID     pipeline;
    struct {
        VulkanBuffer points;
        VulkanBuffer normals;
        VulkanBuffer tangents;
        VulkanBuffer bitangents;
        VulkanBuffer uvs;
    } vbo;
    i32            vertex_count;
    Matrix4        transform;
};

struct Camera {
    Matrix4 projection;

    Vector3 position    = { 0.0f, 0.0f, 10.0f };
    Quaternion rotation = quat_from_euler({ 0.0f, 0.0f, 0.0f });
};

enum CameraId {
    CAMERA_PLAYER,
    CAMERA_DEBUG,
    CAMERA_COUNT
};

struct GameState {
    // TODO(jesper): I don't like this
    bool middle_mouse_pressed = false;
    bool right_mouse_pressed = false;

    Camera cameras[CAMERA_COUNT] = {};
    CameraId active_camera = CAMERA_DEBUG;
    
    struct Lights {
        Vector3 sun_dir = { 0.0f, -100.0f, 3.0f };
        Vector4 sun_color { 1.0f, 1.0f, 1.0f, 1.0f };
    } lights;

    Array<RenderObject> render_objects;
    Array<IndexRenderObject> index_render_objects;

    Vector3 velocity = {};

    i32 *key_state;
};

extern GameState    *g_game;

INTROSPECT struct Resolution
{
    i32 width  = 1280;
    i32 height = 720;
};

INTROSPECT struct VideoSettings
{
    Resolution resolution;

    i16 fullscreen = 0;
    i16 vsync      = 1;
};

INTROSPECT struct Settings
{
    VideoSettings video;
};

Entity entities_add(EntityData data);

void game_init();
void game_quit();
void* game_pre_reload();
void game_reload(void *game_state);
void game_input(InputEvent event);
void game_update_and_render(f32 dt);
void game_begin_frame();

